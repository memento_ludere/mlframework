﻿using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace MLFramework.Prefabs.Editor {
public class PrefabDatabaseWindow : EditorWindow {
    private static string m_NewKey = "";
    private static Vector2 m_ScrollPosition = Vector2.zero;
    private static string m_SearchValue;
    private static SearchField m_SearchBar;

    private static string NewKey {
        get { return m_NewKey; }
        set { m_NewKey = value; }
    }

    private static Vector2 ScrollPosition {
        get { return m_ScrollPosition; }
        set { m_ScrollPosition = value; }
    }

    private static string SearchValue {
        get { return m_SearchValue; }
        set { m_SearchValue = value; }
    }

    public static SearchField SearchBar {
        get {
            if(m_SearchBar == null)
                m_SearchBar = new SearchField();
            return m_SearchBar;
        }
    }
    
    [MenuItem("Window/Prefab Database")]
    public static void Init() {
        PrefabDatabaseWindow myWindow = (PrefabDatabaseWindow) EditorWindow.GetWindow(typeof(PrefabDatabaseWindow));
        myWindow.name = "Prefab Database";
        myWindow.titleContent = new GUIContent("Prefab Database");
        myWindow.Show();
    }

    private void OnEnable() {
        NewKey = "";
    }

    private void OnDisable() {
        NewKey = "";
    }

    private void OnGUI() {
        var menuBar = new Rect(0, 0, position.width, 20f);
        var bodyBar = new Rect(0, 20f, position.width, position.height);

        GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
        GUILayout.BeginHorizontal();
        
        SearchValue = SearchBar.OnToolbarGUI(SearchValue);

        GUILayout.EndHorizontal();
        GUILayout.EndArea();

        GUILayout.BeginArea(bodyBar);
        ScrollPosition = EditorGUILayout.BeginScrollView(ScrollPosition);

        var defaultColor = GUI.backgroundColor;
        
        foreach (var key in PrefabDatabase.GetKeyListSearched(SearchValue)) {
            EditorGUILayout.BeginHorizontal();
            GUI.backgroundColor = defaultColor;
            EditorGUILayout.LabelField(key);

            var actualPrefab = PrefabDatabase.GetPrefab(key);
            var newPrefab = EditorGUILayout.ObjectField(actualPrefab, typeof(GameObject), false) as GameObject;
            if (newPrefab != actualPrefab) {
                PrefabDatabase.SetPrefab(key, newPrefab);
                Undo.RecordObject(PrefabDatabase.Instance, "Prefab Database Modified Prefab Value"); 
                EditorUtility.SetDirty(PrefabDatabase.Instance); 
                Repaint();
            }

            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("X")) {
                PrefabDatabase.DeleteKey(key);
                Undo.RecordObject(PrefabDatabase.Instance, "Prefab Database Deleted Key"); 
                EditorUtility.SetDirty(PrefabDatabase.Instance);
                Repaint();
            }

            EditorGUILayout.EndHorizontal();
        }


        EditorGUILayout.BeginHorizontal();
        GUI.backgroundColor = defaultColor;
        NewKey = EditorGUILayout.TextField("New Key", NewKey);

        GUI.backgroundColor = Color.green;
        if (GUILayout.Button("Add Key") && !string.IsNullOrEmpty(NewKey)) {
            if (!PrefabDatabase.ContainsKey(NewKey)) {
                PrefabDatabase.AddKey(NewKey);
                NewKey = "";
                Undo.RecordObject(PrefabDatabase.Instance, "Prefab Database Added New Key"); 
                EditorUtility.SetDirty(PrefabDatabase.Instance);
                Repaint();
            }
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndScrollView();
        GUILayout.EndArea();
    }
}
}