﻿using UnityEditor;
using UnityEngine;

namespace MLFramework.Prefabs.Editor {
[CustomEditor(typeof(PrefabDatabase))]
public class PrefabDatabaseEditor : UnityEditor.Editor{
    public override void OnInspectorGUI() {
        if (GUILayout.Button("Open Prefab Database")) {
            PrefabDatabaseWindow.Init();
        }
    }
}
}