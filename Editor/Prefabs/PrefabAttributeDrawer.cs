﻿using UnityEditor;
using UnityEngine;

namespace MLFramework.Prefabs.Editor {
[CustomPropertyDrawer(typeof(PrefabAttribute))]
public class PrefabAttributeDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

        var prefabAttribute = attribute as PrefabAttribute;

        var filteredPrefabList = PrefabDatabase.GetKeyList(prefabAttribute.PrefixFilter);

        var actualId = filteredPrefabList.IndexOf(property.stringValue);
        if (actualId < 0)
            actualId = filteredPrefabList.Count;
        
        EditorGUI.LabelField(new Rect(position.x, position.y, 100, position.height), label );
        var newId = EditorGUI.Popup(new Rect(position.x + 100, position.y, position.width-100, position.height), actualId, filteredPrefabList.ToArray());

        if (newId < filteredPrefabList.Count)
            property.stringValue = filteredPrefabList[newId];
        else
            property.stringValue = default(string);
    }
}
}