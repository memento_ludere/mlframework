﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.Pooling {
public class PoolManager : SingletonMonoBehaviour<PoolManager> {
    public const string _LoadPath = "PoolProfiles/";

    private static Dictionary<string, ObjectPool> m_LoadedProfiles;

    public static Dictionary<string, ObjectPool> LoadedProfiles {
        get { return m_LoadedProfiles ?? (m_LoadedProfiles = new Dictionary<string, ObjectPool>()); }
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static void Init() {
        Instance.EnableDontDestroy();
        
        var profiles = Resources.LoadAll(_LoadPath, typeof(PoolProfile));

        foreach (var profile in profiles) {
            LoadProfile(profile as PoolProfile);
        }
    }
    
    public static void LoadProfile(PoolProfile profile) {
        if (LoadedProfiles.ContainsKey(profile.Prefab)) return;

        ObjectPool newPool = new ObjectPool(profile, Instance.transform);
        LoadedProfiles.Add(profile.Prefab, newPool);
    }

    public static GameObject Pull(string prefab, Vector3 position, Quaternion rotation) {
        if (!LoadedProfiles.ContainsKey(prefab))
            throw new Exception("Error, no pool generated");

        return LoadedProfiles[prefab].Pull(position, rotation);
    }

    public static GameObject Pull(string prefab) {
        return Pull(prefab, Vector3.zero, Quaternion.identity);
    }

    public static void Push(string prefab, GameObject instance) {
        if (!LoadedProfiles.ContainsKey(prefab))
            throw new Exception("Error, no pool generated");

        LoadedProfiles[prefab].Push(instance);
    }
}
}