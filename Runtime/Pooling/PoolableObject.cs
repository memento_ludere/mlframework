﻿using UnityEngine;

namespace MLFramework.Pooling {
public class PoolableObject : MonoBehaviour {
    protected string m_Prefab;

    public string Prefab {
        get { return m_Prefab; }
        protected set { m_Prefab = value; }
    }

    public void SetSourcePrefab(string prefab) {
        Prefab = prefab;
    }
    public virtual void OnBeforeDisable() { }
}
}