﻿using System;

namespace MLFramework.Audio {
[Serializable]
public class AudioSettings {
    public float MusicVolume;
    public float EffectsVolume;
}
}