﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.Audio.Components {
public class SFXBindComponent : MonoBehaviour {
    [Serializable]
    public struct SFXBindInfo {
        [Prefab] public string SFXPrefab;
        public Transform Parent;
        
        public SFXBindInfo(string sfxPrefab, Transform parent) {
            if(PrefabManager.GetPrefabReference(sfxPrefab) == null)
                throw  new Exception("No Prefab Found");
        
            SFXPrefab = sfxPrefab;
            Parent = parent;
        }
    }

    public class VFXBindComponent : MonoBehaviour {
        [SerializeField] private List<SFXBindInfo> m_SFXToBind = new List<SFXBindInfo>();

        public List<SFXBindInfo> SFXToBind => m_SFXToBind;

        public void Bind() {
            foreach (var sfxBindInfo in SFXToBind) {
                AudioManager.BindEmitter(sfxBindInfo.SFXPrefab, sfxBindInfo.Parent);
            }
        }
    }
}
}