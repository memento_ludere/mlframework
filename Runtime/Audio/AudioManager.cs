﻿using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.Audio {
public class AudioManager : SingletonMonoBehaviour<AudioManager> {
    private static AudioSettings m_Settings;
    private static List<SFXEmitterController> m_SpawnedSFX;
    private static Dictionary<Transform, List<SFXEmitterController>> m_BoundSFX;


    public static AudioSettings Settings {
        get {
            if(m_Settings == null)
                LoadSettings();
            return m_Settings;
        }
    }

    public static List<SFXEmitterController> SpawnedSFX => m_SpawnedSFX ?? (m_SpawnedSFX = new List<SFXEmitterController>());
    public static Dictionary<Transform, List<SFXEmitterController>> BoundSFX => m_BoundSFX ?? (m_BoundSFX = new Dictionary<Transform, List<SFXEmitterController>>());
    
    [RuntimeInitializeOnLoadMethod]
    private static void Init() {
        Instance.EnableDontDestroy();
        LoadSettings();
    }

    public static void LoadSettings() {
        m_Settings = new AudioSettings();
    }

    public static void SaveSettings() {
        
    }
    
    private void Update() {
        if (SpawnedSFX.Count > 0) {
            Queue<SFXEmitterController> sfxToUnload = new Queue<SFXEmitterController>();
            foreach (var sfx in SpawnedSFX) {
                if (!sfx.IsPlaying()) {
                    sfxToUnload.Enqueue(sfx);
                }
            }

            while (sfxToUnload.Count > 0) {
                var vfx = sfxToUnload.Dequeue();
                SpawnedSFX.Remove(vfx);
                PrefabManager.DestroyPrefab(vfx);
            }
        }
    }

    public static void Play(string prefab, Vector3 position, params KeyValuePair<string, float>[] playParameters) {
        var sfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (sfxInstance) {
            sfxInstance.transform.position = position;
            var sfxEmitter = sfxInstance.GetComponent<SFXEmitterController>();
            sfxEmitter.Play(playParameters);
            SpawnedSFX.Add(sfxEmitter);
        }
    }
    
    public static void Play(string prefab, Transform parent, params KeyValuePair<string, float>[] playParameters) {
        var sfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (sfxInstance) {
            sfxInstance.transform.parent = parent;
            sfxInstance.transform.position = Vector3.zero;
            sfxInstance.transform.rotation = Quaternion.identity;
            sfxInstance.transform.localScale = Vector3.one;
            
            var sfxController = sfxInstance.GetComponent<SFXEmitterController>();
            sfxController.Play(playParameters);
            SpawnedSFX.Add(sfxController);
        }
    }

    public static SFXEmitterController BindEmitter(string prefab, Transform transform, params KeyValuePair<string, float>[] playParameters) {
        if (PrefabManager.GetPrefabReference(prefab) == null || transform == null) return null;

        var sfxInstance = PrefabManager.InstantiatePrefab(prefab);
        if (sfxInstance) {
            var sfxController = sfxInstance.GetComponent<SFXEmitterController>();
            sfxController.Stop();

            sfxInstance.transform.parent = transform;

            sfxInstance.transform.localPosition = Vector3.zero;
            sfxInstance.transform.localEulerAngles = Vector3.zero;
            sfxInstance.transform.localScale = Vector3.one;

            if (!BoundSFX.ContainsKey(transform))
                BoundSFX.Add(transform, new List<SFXEmitterController>());

            BoundSFX[transform].Add(sfxController);
            sfxController.Play(playParameters);
            return sfxController;
        }

        return null;
    }
    
    public static void UnbindEmitter(string prefab, Transform transform, bool unbindAllInstances = false) {
        if (PrefabManager.GetPrefabReference(prefab) == null || transform == null || !BoundSFX.ContainsKey(transform)) return;

        Queue<SFXEmitterController> sfxToUnbind = new Queue<SFXEmitterController>();

        foreach (var sfxInstance in BoundSFX[transform]) {
            if (sfxInstance.Prefab == prefab) {
                sfxToUnbind.Enqueue(sfxInstance);
                if (!unbindAllInstances)
                    break;
            }
        }

        while (sfxToUnbind.Count > 0) {
            var sfxInstance = sfxToUnbind.Dequeue();
            BoundSFX[transform].Remove(sfxInstance);
            PrefabManager.DestroyPrefab(sfxInstance);
        }
    }
    
    public static void UnbindVFXAll(Transform transform) {
        if (transform == null || !BoundSFX.ContainsKey(transform)) return;

        Queue<SFXEmitterController> sfxToUnbind = new Queue<SFXEmitterController>();

        foreach (var vfxInstance in BoundSFX[transform]) {
            sfxToUnbind.Enqueue(vfxInstance);
        }

        while (sfxToUnbind.Count > 0) {
            var sfxInstance = sfxToUnbind.Dequeue();
            BoundSFX[transform].Remove(sfxInstance);
            PrefabManager.DestroyPrefab(sfxInstance);
        }
    }
}
}
