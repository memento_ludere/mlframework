﻿using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Scene.Components {
public class SceneLoadListenComponent : MonoBehaviour{
    [SerializeField] private string m_ScenePath;
    [SerializeField] private UnityEvent m_SceneLoaded;

    private void OnEnable() {
        EventsManager.StartListening(EventID.SceneLoaded, OnSceneLoaded);
    }

    private void OnDisable() {
        EventsManager.StopListening(EventID.SceneLoaded, OnSceneLoaded);
    }

    private void OnSceneLoaded(GenericEventArgs args) {
        var targetScene = (string) args.Args[0];
        if(targetScene == m_ScenePath)
            m_SceneLoaded?.Invoke();
    }
}
}