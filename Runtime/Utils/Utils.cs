﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MLFramework
{
public static class Utils
{
    /// <summary>
    /// Sort an input array of comparable objects from lower to higher
    /// </summary>
    /// <param name="arrayToSort">Array that has to be sorted</param>
    /// <typeparam name="T">Type of the array to sort, it has to implement System.IComparable</typeparam>
    /// <returns>The sorted array</returns>
    /// <exception cref="NullReferenceException"></exception>
    public static T[] Sort<T>(this T[] arrayToSort) where T : IComparable
    {
        if (arrayToSort == null) throw new NullReferenceException("Array to sort is null");

        if (arrayToSort.Length <= 1) return arrayToSort;

        T[] ret = new T[arrayToSort.Length];
        ret[0] = arrayToSort[0];
        int actualArrayLength = 1;

        for (int i = 1; i < arrayToSort.Length; i++)
        {
            T valueToInsert = arrayToSort[i];
            int j = 0;
            while (j < actualArrayLength)
            {
                if (ret[j].CompareTo(valueToInsert) > 0)
                {
                    var swap = ret[j];
                    ret[j] = valueToInsert;
                    valueToInsert = swap;
                }

                j++;
            }

            actualArrayLength++;
            ret[j] = valueToInsert;
        }

        return ret;
    }

    /// <summary>
    /// Sort an input list of comparable objects from lower to higher
    /// </summary>
    /// <param name="listToSort">List that has to be sorted</param>
    /// <typeparam name="T">Type of the list to sort, it has to implement System.IComparable</typeparam>
    /// <returns>The sorted List</returns>
    /// <exception cref="NullReferenceException"></exception>
    public static List<T> Sort<T>(this List<T> listToSort) where T : IComparable
    {
        if (listToSort == null) throw new NullReferenceException("List to sort is null");

        if (listToSort.Count <= 1) return listToSort;

        List<T> ret = new List<T>();
        ret.Add(listToSort[0]);

        for (int i = 1; i < listToSort.Count; i++)
        {
            T valueToInsert = listToSort[i];
            bool inserted = false;
            for (int j = 0; j < ret.Count; j++)
            {
                if (ret[j].CompareTo(valueToInsert) > 0)
                {
                    ret.Insert(j, valueToInsert);
                    inserted = true;
                    break;
                }
            }

            if (!inserted)
                ret.Add(valueToInsert);
        }

        return ret;
    }

    /// <summary>
    /// Get an array with all types inherited from a base type
    /// </summary>
    /// <param name="baseType">The base type to search for</param>
    /// <returns>The inherited types</returns>
    public static Type[] GetInheritedTypes(Type baseType)
    {
        List<Type> foundTypes = new List<Type>();
        foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (var type in a.GetTypes())
            {
                foundTypes.Add(type);
            }
        }

        //System.Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
        var types = foundTypes.ToArray();
        System.Type[] possible = (from System.Type type in types where type.IsSubclassOf(baseType) select type).ToArray();
        return possible;
    }

    public static float Remap(float x, float x1, float x2, float y1, float y2)
    {
        float m = (y2 - y1) / (x2 - x1);
        float c = y1 - m * x1;
        return m * x + c;
    }

    /// <summary>
    /// Shuffle the elements of a list
    /// </summary>
    /// <param name="input">List to shuffle</param>
    /// <typeparam name="T">Type of the lit to shuffle</typeparam>
    /// <returns>The shuffled list</returns>
    public static List<T> Shuffle<T>(this List<T> input)
    {
        var ret = new List<T>();
        while (input.Count > 0)
        {
            var selected = input[Random.Range(0, input.Count)];
            ret.Add(selected);
            input.Remove(selected);
        }

        return ret;
    }

    /// <summary>
    /// Shuffle the elements of a list
    /// </summary>
    /// <param name="input">List to shuffle</param>
    /// <param name="randomizer">The randomizer to shuffle tje list</param>
    /// <typeparam name="T">Type of the lit to shuffle</typeparam>
    /// <returns>The shuffled list</returns>
    public static List<T> Shuffle<T>(this List<T> input, System.Random randomizer)
    {
        var ret = new List<T>();
        while (input.Count > 0)
        {
            var selected = input[randomizer.Next(0, input.Count)];
            ret.Add(selected);
            input.Remove(selected);
        }

        return ret;
    }

#if UNITY_EDITOR
    public static LayerMask LayerMaskField(Rect rect, string label, LayerMask layerMask)
    {
        List<string> layers = new List<string>();
        List<int> layerNumbers = new List<int>();

        for (int i = 0; i < 32; i++)
        {
            string layerName = LayerMask.LayerToName(i);
            if (layerName != "")
            {
                layers.Add(layerName);
                layerNumbers.Add(i);
            }
        }

        int maskWithoutEmpty = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if (((1 << layerNumbers[i]) & layerMask.value) > 0)
                maskWithoutEmpty |= (1 << i);
        }

        maskWithoutEmpty = EditorGUI.MaskField(rect, label, maskWithoutEmpty, layers.ToArray());
        int mask = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if ((maskWithoutEmpty & (1 << i)) > 0)
                mask |= (1 << layerNumbers[i]);
        }

        layerMask.value = mask;
        return layerMask;
    }

    public static LayerMask LayerMaskField(string label, LayerMask layerMask)
    {
        List<string> layers = new List<string>();
        List<int> layerNumbers = new List<int>();

        for (int i = 0; i < 32; i++)
        {
            string layerName = LayerMask.LayerToName(i);
            if (layerName != "")
            {
                layers.Add(layerName);
                layerNumbers.Add(i);
            }
        }

        int maskWithoutEmpty = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if (((1 << layerNumbers[i]) & layerMask.value) > 0)
                maskWithoutEmpty |= (1 << i);
        }

        maskWithoutEmpty = EditorGUILayout.MaskField(label, maskWithoutEmpty, layers.ToArray());
        int mask = 0;
        for (int i = 0; i < layerNumbers.Count; i++)
        {
            if ((maskWithoutEmpty & (1 << i)) > 0)
                mask |= (1 << layerNumbers[i]);
        }

        layerMask.value = mask;
        return layerMask;
    }
#endif
}
}