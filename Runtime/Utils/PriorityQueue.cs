﻿using System;
using System.Collections.Generic;

namespace MLFramework
{
    public class PriorityQueue<T> where T : IComparable
    {
        private List<T> m_Queue;

        public int Count => m_Queue.Count;

        public T this[int index]
        {
            get
            {
                return m_Queue[index];
            }
            set
            {
                m_Queue[index] = value;
            }
        }

        public PriorityQueue()
        {
            m_Queue = new List<T>();
        }

        /// <summary>
        /// Enqueue an object inside the queue and sort it inside the queue.
        /// </summary>
        /// <param name="obj">The object to add to the queue.</param>
        public void Enqueue(T obj)
        {
            m_Queue.Add(obj);
            m_Queue = Utils.Sort(m_Queue);
        }

        /// <summary>
        /// Dequeue the first object of the queue.
        /// </summary>
        /// <returns>The first object of the queue.</returns>
        public T Dequeue()
        {
            if (m_Queue.Count <= 0)
                return default(T);

            var ret = m_Queue[0];
            m_Queue.RemoveAt(0);
            return ret;
        }

        /// <summary>
        /// Removes a specified object from the queue.
        /// </summary>
        /// <param name="objectToRemove">The object that has to be removed from the queue.</param>
        public void Remove(T objectToRemove)
        {
            if (m_Queue.Contains(objectToRemove))
                m_Queue.Remove(objectToRemove);
        }

        /// <summary>
        /// Removes all object of a specified type.
        /// </summary>
        /// <param name="objectsToRemove">The type of objects that have to be removed from the queue.</param>
        public void RemoveAll(T objectsToRemove)
        {
            while (m_Queue.Contains(objectsToRemove))
                m_Queue.Remove(objectsToRemove);
        }

        /// <summary>
        /// Returns the first element of the queue without dequeueing it from the queue itself.
        /// </summary>
        /// <returns></returns>
        public T FirstElement()
        {
            if (m_Queue.Count <= 0)
                return default(T);

            var ret = m_Queue[0];
            return ret;
        }
    }
}