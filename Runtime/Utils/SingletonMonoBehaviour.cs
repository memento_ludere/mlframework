﻿using System;
using UnityEngine;

namespace MLFramework {
/// <summary>
/// Simple generic implementation of the singleton pattern for MonoBehavior objects
/// </summary>
public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {
#region Properties

    protected static T m_Instance;
    public static T Instance {
        get {
            if (m_Instance != null) return m_Instance;
            m_Instance = FindObjectOfType<T>();
            if (m_Instance != null) return m_Instance;
            GameObject go = new GameObject("Singleton - " + typeof(T).Name);
            m_Instance = go.AddComponent<T>();
            if (m_Instance == null) {
                throw new System.Exception("Can't generate " + typeof(T).Name + " Singleton");
            }

            return m_Instance;
        }
    }

    private bool m_DontDestroy = false;
    public bool DontDestroy {
        get { return m_DontDestroy; }
    }

#endregion

#region MonoBehaviour Methods

    protected virtual void Awake() {
        if (Instance != null && Instance != this) {
            Destroy(this.gameObject);
        }
    }

#endregion

#region Public Methods

    public void EnableDontDestroy() {
        if (m_DontDestroy) return;
        DontDestroyOnLoad(this.gameObject);
        m_DontDestroy = true;
    }

#endregion
}
}