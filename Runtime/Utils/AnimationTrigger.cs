﻿using UnityEngine;

namespace MLFramework.Utility
{
public class AnimationTrigger : MonoBehaviour
{
    [SerializeField] private Animator m_TargetAnimator;
    [SerializeField] private string m_TriggerToCall;

    public void CallTrigger()
    {
        m_TargetAnimator.SetTrigger(m_TriggerToCall);
    }
}
}
