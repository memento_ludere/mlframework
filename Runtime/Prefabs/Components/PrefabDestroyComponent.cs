﻿using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.Prefabs.Components {
public class PrefabDestroyComponent : MonoBehaviour{
    [SerializeField] private List<GameObject> m_PrefabsToDestroy = new List<GameObject>();

    public List<GameObject> PrefabsToDestroy => m_PrefabsToDestroy;

    public void Destroy() {
        foreach (var prefab in PrefabsToDestroy) {
            PrefabManager.DestroyPrefab(prefab);
        }
    }
}
}