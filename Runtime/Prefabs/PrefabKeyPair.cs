﻿using System;
using UnityEngine;

namespace MLFramework.Prefabs {
/// <summary>
/// Serializable class use to save references between a Prefab and his key
/// </summary>
[Serializable]
public class PrefabKeyPair : IComparable {
    [SerializeField] private string m_Key;
    [SerializeField] private GameObject m_Prefab;

    public string Key {
        get { return m_Key; }
        set { m_Key = value; }
    }

    public GameObject Prefab {
        get { return m_Prefab; }
        set { m_Prefab = value; }
    }

    public PrefabKeyPair(string key, GameObject prefab = null) {
        Key = key;
        Prefab = prefab;
    }

    public int CompareTo(object obj) {
        var other = obj as PrefabKeyPair;
        if (other != null)
            return String.Compare(Key, other.Key, StringComparison.Ordinal);

        throw new InvalidCastException("Can't cast object to type \"PrefabKeyPair\"");
    }
}
}