﻿using System;
using System.Collections.Generic;
using MLFramework.Pooling;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MLFramework.Prefabs
{
public static class PrefabManager
{
    public static GameObject GetPrefabReference(string prefabKey)
    {
        if (!PrefabDatabase.ContainsKey(prefabKey))
            throw new Exception("Prefab Not Found");

        return PrefabDatabase.GetPrefab(prefabKey);
    }

    public static GameObject InstantiatePrefab(string prefabKey)
    {
        if (!PrefabDatabase.ContainsKey(prefabKey))
            throw new Exception("Prefab Not Found " + prefabKey);

        if (PoolManager.LoadedProfiles.ContainsKey(prefabKey))
            return PoolManager.Pull(prefabKey);

        return GameObject.Instantiate(PrefabDatabase.GetPrefab(prefabKey));
    }

    public static void DestroyPrefab(GameObject obj, bool destroyImmediate = false)
    {
        var poolableObject = obj.GetComponent<PoolableObject>();
        if (poolableObject)
        {
            PoolManager.Push(poolableObject.Prefab, obj);
            return;
        }

        if (destroyImmediate)
            GameObject.DestroyImmediate(obj);
        else
            GameObject.Destroy(obj);
    }

    public static void DestroyPrefab(PoolableObject obj)
    {
        PoolManager.Push(obj.Prefab, obj.gameObject);
    }

    public static void DestroyChildren(Transform parent, bool destroyImmediate = false)
    {
        var childrenToDestroy = new Queue<GameObject>();

        foreach (Transform child in parent)
        {
            childrenToDestroy.Enqueue(child.gameObject);
        }

        while (childrenToDestroy.Count > 0)
        {
            DestroyPrefab(childrenToDestroy.Dequeue(), destroyImmediate);
        }
    }
}
}