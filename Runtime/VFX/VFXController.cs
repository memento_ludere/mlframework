﻿using System;
using MLFramework.Pooling;
using UnityEngine;

namespace MLFramework.VFX {
public abstract class VFXController : PoolableObject {
    [SerializeField] protected float m_LifeTime;
    [SerializeField] protected bool m_Loop = false;

    protected float m_Timer;
    protected bool m_Running;

    protected void Update() {
        if (m_Running) {
            m_Timer += Time.deltaTime;
            if (m_Timer >= m_LifeTime)
            {
                if (m_Loop)
                    Play();
                else
                    Stop();
            }
        }
    }

    public virtual void Play() {
        m_Timer = 0f;
        m_Running = true;
    }

    public virtual void Stop() {
        m_Running = false;
    }

    public bool IsPlaying() {
        return m_Running;
    }

    private void OnDisable() {
        m_Running = false;
    }
}
}