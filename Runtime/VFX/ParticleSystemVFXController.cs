﻿using UnityEngine;

namespace MLFramework.VFX {
public class ParticleSystemVFXController : VFXController {
    [SerializeField] private ParticleSystem m_Particles;

    public ParticleSystem Particles {
        get {
            if (m_Particles == null) {
                m_Particles = GetComponent<ParticleSystem>();
                if (m_Particles == null)
                    m_Particles = GetComponentInChildren<ParticleSystem>();
            }

            return m_Particles;
        }
    }

    public override void Play() {
        base.Play();
        if (Particles != null) 
            Particles.Play(true);
    }

    public override void Stop() {
        base.Stop();
        if(Particles!=null)
            Particles.Stop(true);
    }
}
}