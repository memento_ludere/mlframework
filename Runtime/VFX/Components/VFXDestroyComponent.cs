﻿using System.Collections.Generic;
using UnityEngine;

namespace MLFramework.VFX.Components {
public class VFXDestroyComponent : MonoBehaviour {
    [SerializeField] public List<VFXController> m_VFXToDestroy = new List<VFXController>();

    public List<VFXController> VFXToDestroy => m_VFXToDestroy;

    public void Destroy() {
        foreach (var vfxController in VFXToDestroy) {
            vfxController.Stop();
        }
    }
}
}