﻿using System;
using System.Collections.Generic;
using MLFramework.Prefabs;
using UnityEngine;

namespace MLFramework.VFX.Components {
[Serializable]
public struct VFXSpawnInfo {
    [Prefab] public string VFXPrefab;
    public Vector3 Position;
    public Vector3 Rotation;
    public Transform Parent;

    public VFXSpawnInfo(string vfxPrefab, Vector3 position, Vector3 rotation, Transform parent = null) {
        if(PrefabManager.GetPrefabReference(vfxPrefab) == null)
            throw new Exception("No Prefab Found");
        
        VFXPrefab = vfxPrefab;
        Position = position;
        Rotation = rotation;
        Parent = parent;
    }
} 

public class VFXSpawnComponent : MonoBehaviour {
    [SerializeField] private List<VFXSpawnInfo> m_VFXToSpawn = new List<VFXSpawnInfo>();

    public List<VFXSpawnInfo> VFXToSpawn => m_VFXToSpawn;

    public void Spawn() {
        foreach (var vfxSpawnInfo in VFXToSpawn) {
            if(vfxSpawnInfo.Parent != null)
                VFXManager.SpawnVFX(vfxSpawnInfo.VFXPrefab, vfxSpawnInfo.Parent);
            else
                VFXManager.SpawnVFX(vfxSpawnInfo.VFXPrefab, vfxSpawnInfo.Position, Quaternion.Euler(vfxSpawnInfo.Rotation));
        }
    }
}
}