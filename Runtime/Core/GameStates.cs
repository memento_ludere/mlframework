﻿#if DEFAULT_GAME_STATES
namespace MLFramework.Core {
public enum GameStates {
    MainMenu,
    SettingsMenu,
    Gameplay
}
}
#endif