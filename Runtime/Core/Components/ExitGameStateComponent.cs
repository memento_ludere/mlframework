﻿using MLFramework.Events;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Core.Components {
public class ExitGameStateComponent : MonoBehaviour {
    [SerializeField] private GameStates m_TargetState;
    [SerializeField] private UnityEvent m_ExitState;

    private void OnEnable() {
        EventsManager.StartListening(EventID.GameStateExit, OnStateChanged);
    }

    private void OnDisable() {
        EventsManager.StartListening(EventID.GameStateExit, OnStateChanged);
    }

    private void OnStateChanged(GenericEventArgs args) {
        var state = (GameStates) args.Args[0];

        if (state == m_TargetState) {
            m_ExitState?.Invoke();
        }
    }
}
}