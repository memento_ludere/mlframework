﻿using UnityEngine;

namespace MLFramework.Core.Components {
public class ChangeGameStateComponent : MonoBehaviour {
    [SerializeField] private GameStates m_TargetState;

    public void ChangeState() {
        GameManager.ChangeState(m_TargetState);
    }
}
}