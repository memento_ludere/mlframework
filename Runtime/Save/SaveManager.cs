﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace MLFramework.Save
{
public static class SaveManager
{
    public static void SaveData<T>(string fileName, T data)
    {
        var bf = new BinaryFormatter();
        var fs = File.Create(Application.persistentDataPath + "/" + fileName);
        bf.Serialize(fs, data);
        fs.Close();
    }

    public static T LoadData<T>(string fileName)
    {
        var result = default(T);

        if (File.Exists(Application.persistentDataPath + "/" + fileName))
        {
            var bf = new BinaryFormatter();
            var fs = File.Open(Application.persistentDataPath + "/" + fileName, FileMode.Open);
            result = (T) bf.Deserialize(fs);
            fs.Close();
        }
        
        return result;
    }
}
}