﻿using System;
using UnityEngine;

namespace MLFramework.Events.Components {
[Obsolete]
public class EventTriggerComponent : MonoBehaviour {
    [SerializeField] private EventID m_EventToTrigger;

    public void Trigger() {
        EventsManager.TriggerEvent(m_EventToTrigger);
    }
}
}