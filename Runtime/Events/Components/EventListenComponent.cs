﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MLFramework.Events.Components
{
[Obsolete]
    public class EventListenComponent : MonoBehaviour
    {
        [SerializeField] private EventID m_EventToListen;
        [SerializeField] private UnityEvent m_ListenTarget;

        private void OnEnable()
        {
            EventsManager.StartListening(m_EventToListen, OnEventTriggered);
        }

        private void OnDisable()
        {
            EventsManager.StopListening(m_EventToListen, OnEventTriggered);
        }

        private void OnEventTriggered(GenericEventArgs args)
        {
            m_ListenTarget.Invoke();
        }
    }
}